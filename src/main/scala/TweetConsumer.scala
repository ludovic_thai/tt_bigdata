import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.tree.DecisionTree
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, date_format, to_timestamp}
import org.apache.spark.sql.types.TimestampType
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.elasticsearch.spark.sql.sparkDatasetFunctions

object TweetConsumer {
  import Extractors._

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)

    if (args.length == 0) {
      System.out.println("Enter the name of the topic")
      return;
    }

    // Creating spark session and spark context
    val conf = new SparkConf().setAppName("SparkKafkaStreaming").setMaster("local[*]")
    val ssc = new StreamingContext(conf, Seconds(10))
    val sc = ssc.sparkContext
    val spark = SparkSession.builder.config(sc.getConf).getOrCreate()

    import spark.implicits._

    val topic = args(0)
    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> "localhost:9092",
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> "group1",
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )

    // --- TRAINING THE DECISION TREE ---
    // Loading data
    val dataRddRaw = sc
      .textFile("res/data.txt")
      .map(l=>l.split(","))
    val dataRDD = dataRddRaw
      .mapPartitionsWithIndex {
        (idx, iter) => if (idx == 0) iter.drop(1) else iter
      }

    // Preparing data
    val doubleData = dataRDD
      .map(l => l.map(elm => elm.toDouble))
    val parsedData=doubleData.map(l=>{
      val features=Vectors.dense(l(1),l(2),l(3),l(4),l(5),l(6),l(7),l(8),l(9))
      LabeledPoint(l(10),features)
    }).cache()

    // Training Data
    val numClasses = 2
    val categoricalFeaturesInfo = Map[Int, Int]()
    val impurity = "gini"
    val maxDepth = 5
    val maxBins = 32

    val Array(trainData, testData) = parsedData
      .randomSplit(Array(0.7,0.3))
    val dtModel = DecisionTree
      .trainClassifier(trainData, numClasses, categoricalFeaturesInfo,
        impurity, maxDepth, maxBins)


    // --- PROCESSING TWEET STREAMS ---
    val topics = Array(topic)
    val stream = KafkaUtils.createDirectStream[String, String](
      ssc,
      PreferConsistent,
      Subscribe[String, String](topics, kafkaParams)
    )

    val records = stream.map(record => (record.key, record.value))
    records.foreachRDD(rdd => {
      // parsing the record
      val parsedRDD = rdd
        .map(rdd => {
          val statusId = extractStatusId(rdd._2)
          val userId = extractUserId(rdd._2)
          val username = extractUsername(rdd._2)
          val tweetText = extractTweetText(rdd._2)
          val createdAt = extractCreatedAt(rdd._2)
          val tweetRetweetCount = extractTweetRetweetedCount(rdd._2)
          val tweetFavoritedCount = extractTweetFavoritedCount(rdd._2)
          val userStatusesCount = extractUserStatusesCount(rdd._2)
          val userFollowersCount = extractUserFollowersCount(rdd._2)
          val userFriendsCount = extractUserFriendsCount(rdd._2)
          val userFavoritesCount = extractUserFavoritesCount(rdd._2)
          val userProtected = extractUserProtected(rdd._2)
          val userVerified = extractUserVerified(rdd._2)

          val userData = Vectors.dense(tweetRetweetCount, tweetFavoritedCount,
            userStatusesCount, userFollowersCount, userFriendsCount,
            userFavoritesCount, userProtected, userVerified, 0)
          val boltUser = dtModel.predict(userData)

          (statusId, userId, username, tweetText, createdAt, tweetRetweetCount,
          tweetFavoritedCount, userStatusesCount, userFollowersCount, userFriendsCount,
          userFavoritesCount , userProtected, userVerified, boltUser, rdd._2)
        })

      // Record to dataframe
      val dfRecord = parsedRDD
        .toDF(
          "statusId",
          "userId",
          "username",
          "tweetText",
          "createdAt",
          "tweetRetweetedCount",
          "tweetFavoritedCount",
          "userStatusesCount",
          "userFollowersCount",
          "userFriendsCount",
          "userFavoritesCount",
          "userProtected",
          "userVerified",
          "boltUser",
          "recordData"
        )

      val dfRecordClean = dfRecord
        .withColumn("createdAt",to_timestamp($"createdAt", "MM/dd/yyyy HH:mm:ss"))
        .withColumn("createdAt", date_format(col("createdAt") cast TimestampType, "yyyy-MM-dd'T'HH:mm:ss.SSSZ"))

      parsedRDD.take(10).foreach(println)
      dfRecordClean.show(10)

      // Saving the dataframe to ES
      dfRecordClean.saveToEs("tweets")
    })

    ssc.start()
    ssc.awaitTermination()
  }
}