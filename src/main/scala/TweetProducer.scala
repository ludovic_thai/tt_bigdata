import java.util.Properties
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}

object TweetProducer {
  def main(args: Array[String]): Unit = {
    import DateConverter._

    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("twitter4j.oauth.consumerKey",
      "MDq37ux5pGGCdsVKV5ALwgDsu")
    System.setProperty("twitter4j.oauth.consumerSecret",
      "QQLjYKepaAZGW4mkufHaKHrKfoYkFzEGJxRy4JDo9G8qVsYNrm")
    System.setProperty("twitter4j.oauth.accessToken",
      "1384045555643346947-rVquyUQlqSn7gv2PPgDKBYt3EHsP2z")
    System.setProperty("twitter4j.oauth.accessTokenSecret",
      "H4FuDm1d2sL8xCNCh9qtumWFCrfXt6s7Y7QK4isnTy4nJ")

    val topicName = args(0)

    // Creating spark session and spark context
    val spark = SparkSession.builder()
      .master("local[2]")
      .appName("sample-structured-streaming")
      .getOrCreate()
    val sc = spark.sparkContext
    val ssc = new StreamingContext(sc, Seconds(5))

    // Creating the tweet stream
    val tweetStream = TwitterUtils
      .createStream(ssc, None)
      .filter(tweet => tweet.getLang == "en")
      .map(tweet => (
        tweet.getText,
        tweet.getUser,
        tweet.getRetweetCount,
        tweet.getFavoriteCount,
        tweet.getId,
        dateToString(tweet.getCreatedAt)
      ))


    // Set the scene for my producer
    val props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")
    props.put("acks", "all")
    props.put("retries", "0")
    props.put("batch.size", "16384");
    props.put("buffer.memory", "33554432");
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    // Start processing tweets
    tweetStream.foreachRDD(rdd => {
      rdd.foreachPartition(l => {
        var producer = new KafkaProducer[String, String](props)
        l.foreach { tweet =>
          val text = tweet._1
            .replaceAll("\\n", " ")
            .replaceAll("\\r", " ")
          val user = tweet._2.toString
            .replaceAll("\\n", " ")
            .replaceAll("\\r", " ")
          val retweetCount = tweet._3
          val favoriteCount = tweet._4
          val statusId = tweet._5
          val createdAt = tweet._6

          val value = String.format("{\"text\": %s, \"user\": %s, \"retweetCount\": %s, \"favoriteCount\":" +
            " %s, \"statusId\": %s, \"createdAt\": %s}", text, user, retweetCount.toString, favoriteCount.toString,
            statusId.toString, createdAt)

          val record = new ProducerRecord(topicName, "", value)
          producer.send(record)
        }
      })
    })

    ssc.start()
    ssc.awaitTermination()
  }
}