import java.util.Date
import java.text.DateFormat

object DateConverter {
  def dateToString(date:Date): String = {
    import java.text.SimpleDateFormat
    val pattern = "MM/dd/yyyy HH:mm:ss"
    val df = new SimpleDateFormat(pattern)
    val dateString = df.format(date)
    return dateString
  }

  def stringToDate(dateStr:String): Date = {
    import java.text.SimpleDateFormat
    val formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
    val date = formatter.parse(dateStr)
    return date
  }

}
