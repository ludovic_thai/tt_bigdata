object Extractors {
  def extractTweetRetweetedCount(recordValue:String): Int = {
    val patternRequest = """.*\"retweetCount\": ([0-9]+),.*""".r
    recordValue match {
      case patternRequest(request) => return request.toInt
      case _ => return -1
    }
  }

  def extractTweetFavoritedCount(recordValue:String): Int = {
    val patternRequest = """.*\"favoriteCount\": ([0-9]+),.*""".r
    recordValue match {
      case patternRequest(request) => return request.toInt
      case _ => return -1
    }
  }

  def extractUserStatusesCount(recordValue:String): Int = {
    val patternRequest = """.*statusesCount=([0-9]+),.*""".r
    recordValue match {
      case patternRequest(request) => return request.toInt
      case _ => return -1
    }
  }

  def extractUserFollowersCount(recordValue:String): Int = {
    val patternRequest = """.*followersCount=([0-9]+),.*""".r
    recordValue match {
      case patternRequest(request) => return request.toInt
      case _ => return -1
    }
  }

  def extractUserFriendsCount(recordValue:String): Int = {
    val patternRequest = """.*friendsCount=([0-9]+),.*""".r
    recordValue match {
      case patternRequest(request) => return request.toInt
      case _ => return -1
    }
  }

  def extractUserFavoritesCount(recordValue:String): Int = {
    val patternRequest = """.*favouritesCount=([0-9]+),.*""".r
    recordValue match {
      case patternRequest(request) => return request.toInt
      case _ => return -1
    }
  }

  def extractUserProtected(recordValue:String): Int = {
    val patternRequestFalse = """.*(isProtected=false),.*""".r
    val patternRequestTrue = """.*(isProtected=true),.*""".r
    recordValue match {
      case patternRequestFalse(request) => return 0
      case patternRequestTrue(request) => return 1
      case _ => return -1
    }
  }

  def extractUserVerified(recordValue:String): Int = {
    val patternRequestFalse = """.*(isVerified=false),.*""".r
    val patternRequestTrue = """.*(isVerified=true),.*""".r
    recordValue match {
      case patternRequestFalse(request) => return 0
      case patternRequestTrue(request) => return 1
      case _ => return -1
    }
  }

  def extractTweetText(recordValue:String): String = {
    val patternRequest = """\{\"text\": (.*), \"user\".*""".r
    recordValue match {
      case patternRequest(request) => return request
      case _ => return ""
    }
  }

  def extractStatusId(recordValue:String): Double = {
    val patternRequest = """.*\"statusId\": ([0-9]+), .*""".r
    recordValue match {
      case patternRequest(request) => return request.toDouble
      case _ => return -1
    }
  }

  def extractUsername(recordValue:String): String = {
    val patternRequest = """.*name=\'(.*)\', email.*""".r
    recordValue match {
      case patternRequest(request) => return request
      case _ => return ""
    }
  }

  def extractUserId(recordValue:String): Double = {
    val patternRequest = """.*id=([0-9]+), name.*""".r
    recordValue match {
      case patternRequest(request) => return request.toDouble
      case _ => return -1
    }
  }

  def extractCreatedAt(recordValue:String): String = {
    val patternRequest = """.*\"createdAt\": (.*)\}""".r
    recordValue match {
      case patternRequest(request) => return request
      case _ => return ""
    }
  }

}
