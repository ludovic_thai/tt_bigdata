<!--suppress HtmlRequiredAltAttribute -->
<a href="https://taltech.ee/en/">
<img src="https://i.err.ee/smartcrop?type=optimize&width=672&aspectratio=16%3A10&url=https%3A%2F%2Fs.err.ee%2Fphoto%2Fcrop%2F2019%2F02%2F14%2F600273ha594.jpg" width="256" >
</a>


REAL TIME BIG DATA ANALYSIS FINAL PROJECT 
=========================================

Ludovic Thai - luthai@ttu.ee

**The video demo of the project is available in 'demo/video/BigDataDemo.mp4'**

# Installation (Mac OS)
## Required software and libraries
- Java 8
- SBT
- Kafka 2.7 or higher
- Zookeeper 
- Elastic search
- Kibanna 

## Installation with Homebrew
```shell
brew tap homebrew/cask-versions
brew update
brew tap caskroom/cask
brew tap adoptopenjdk/openjdk
brew cask install adoptopenjdk8
brew install kafka
brew install zookeeper
brew installa kibanna 
brew install sbt
```

# Run
1. Start Zookeeper, Kafka and ES in separate terminals
   `$ ./command/startZookeeper.sh`
   `$ ./command/startKafka.sh`
   `$ elasticsearch` (to check if elastic is running, use `curl http://localhost:9200` )
2. Start the producer directly in IntelliJ with the program argument `tweets` or using sbt
   ```shell
   $ sbt
   sbt> run tweets
   ```
   And select the producer `TweetProducer`.
3. Start the consumer directly in IntelliJ with the program argument `tweets` or using sbt
   ```shell
   $ sbt
   sbt> run tweets
   ```
   And select the producer `TweetConsumer`.
4. Start Kibana
   `kibana`
   Kibana status: `http://localhost:5601/status`
   Access Kibana : `http://localhost:5601/app/home`
   
# Stop
```shell
$ ./command/stopZookeeper.sh
$ ./command/stopKafka.sh
$ ./command/stopAll
```

# Other
Data sample:
```json
{"text": @tollmetron oh hell yes, already demographic tested, "user": UserJSONImpl{id=3079674711, name='happie', email='null', screenName='kungsrubinen', location='null', description='🐯stella / 24 / she-her /🇫🇮/ lesbian / girl next door (from hell) / watch tiger & bunny (2011)🐯 art only acco @sv3ngalia', isContributorsEnabled=false, profileImageUrl='http://pbs.twimg.com/profile_images/1394255629875912706/pLVpDqNp_normal.jpg', profileImageUrlHttps='https://pbs.twimg.com/profile_images/1394255629875912706/pLVpDqNp_normal.jpg', isDefaultProfileImage=false, url='http://svengalia.carrd.co', isProtected=false, followersCount=271, status=null, profileBackgroundColor='C0DEED', profileTextColor='333333', profileLinkColor='F58EA8', profileSidebarFillColor='DDEEF6', profileSidebarBorderColor='C0DEED', profileUseBackgroundImage=true, isDefaultProfile=false, showAllInlineMedia=false, friendsCount=275, createdAt=Sun Mar 08 18:48:08 EET 2015, favouritesCount=67153, utcOffset=-1, timeZone='null', profileBackgroundImageUrl='http://abs.twimg.com/images/themes/theme1/bg.png', profileBackgroundImageUrlHttps='https://abs.twimg.com/images/themes/theme1/bg.png', profileBackgroundTiled=false, lang='null', statusesCount=36276, isGeoEnabled=true, isVerified=false, translator=false, listedCount=11, isFollowRequestSent=false, withheldInCountries=[]}, "retweetCount": 0, "favoriteCount": 0, "statusId": 1397528566196326403, "createdAt": Wed May 26 15:22:29 EEST 2021}
```
