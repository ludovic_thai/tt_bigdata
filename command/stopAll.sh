#!/bin/sh
echo "Stopping Kafka"
kafka-server-stop
echo "Stopping Zookeeper"
zookeeper-server-stop
